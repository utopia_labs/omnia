﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace QLib
{
    public class SimpleSettingsManager
    {
        string file;

        XmlSerializer serializer;

        public Dictionary<string, string> Dict = new Dictionary<string, string>();

        public SimpleSettingsManager(string file = "default.xml", string root = "Default")
        {
            serializer = new XmlSerializer(typeof(SettingsItem[]), 
                new XmlRootAttribute() { ElementName = root });
            this.file = file;
            Load();
        }

        public void Load()
        {
            if (!File.Exists(file))
            {
                return;
            }
            FileStream settings = new FileStream(file, FileMode.Open);
            Dict = ((SettingsItem[])serializer.Deserialize(settings))
               .ToDictionary(i => i.id, i => i.value);
            settings.Close();
        }

        public void Commit()
        {
            FileStream settings = new FileStream(file, FileMode.Create);
            serializer.Serialize(settings,
              Dict.Select(kv => new SettingsItem() { id = kv.Key, value = kv.Value }).ToArray());
            settings.Close();
        }

        public void Set(object key, object value)
        {
            Dict[key.ToString()] = value.ToString();
            Commit();
        }

        public string Get(object key)
        {
            if (Dict.Keys.Contains(key.ToString()))
            {
                return Dict[key.ToString()];
            }
            else
            {
                Dict[key.ToString()] = "";
                Commit();
                return "";
            }
        }

        public ulong GetN(object key) => ulong.Parse(Get(key));
        public bool GetB(object key) => bool.Parse(Get(key));
    }

    public class SettingsItem
    {
        [XmlAttribute]
        public string id;
        [XmlAttribute]
        public string value;
    }
}
