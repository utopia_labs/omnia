﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Win32;

namespace QLib
{
    public static class eRegistry
    {
        public static T? sGetValue<T>(string key, string value) where T : struct
        {
            return Registry.GetValue(key, value, null) as T?;
        }

        public static T GetValue<T>(string key, string value) where T : class
        {
            return Registry.GetValue(key, value, null) as T;
        }

        public static void SetValue<T>(string key, string value, T set)
        {
            Registry.SetValue(key, value, set);
        }
    }
}
