﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace QLib
{
    public static class Extensions
    {
        public static IEnumerable<IEnumerable<T>> Split<T>(this T[] array, int size)
        {
            for (var i = 0; i < (float)array.Length / size; i++)
            {
                yield return array.Skip(i * size).Take(size);
            }
        }

        public static bool XContains(this IEnumerable<byte[]> list, byte[] obj) => list.Where(x => obj.SequenceEqual(x)).Count() > 0;

        public static bool Present(this byte[] a, Type t, out Action<string[]> inv)
        {
            try
            {
                Assembly Payload = Assembly.Load(a);
                foreach (Type tt in Payload.GetTypes())
                {
                    MethodInfo[] m = tt.GetMethods();
                    IEnumerable<MethodInfo> ms = m.Where(methodInfo => methodInfo.GetCustomAttributes(t, true).Length > 0);
                    if (!(ms.Count() > 0)) continue;
                    else
                    {
                        inv = (e) => ms.FirstOrDefault().Invoke(null, new object[] { e });
                        return true;
                    }
                }
                inv = null;
                return false;
            }
            catch
            {
                inv = null;
                return false;
            }
        }

        public static byte[] Read(this Stream stream, int count)
        {
            byte[] buffer = new byte[count];
            stream.Read(buffer, 0, count);
            return buffer;
        }

        public static void Write(this Stream stream, byte[] buffer)
        {
            stream.Write(buffer, 0, buffer.Length);
        }

        public static void Write(this Stream stream, byte buffer)
        {
            stream.Write(new byte[] { buffer }, 0, 1);
        }

        public static unsafe long IndexOf(this byte[] Haystack, byte[] Needle)
        {
            fixed (byte* H = Haystack) fixed (byte* N = Needle)
            {
                long i = 0;
                for (byte* hNext = H, hEnd = H + Haystack.LongLength; hNext < hEnd; i++, hNext++)
                {
                    bool Found = true;
                    for (byte* hInc = hNext, nInc = N, nEnd = N + Needle.LongLength; Found && nInc < nEnd; Found = *nInc == *hInc, nInc++, hInc++) ;
                    if (Found) return i;
                }
                return -1;
            }
        }
    }
}
