﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace QLib
{
    public static class Utility
    {
        public static Type TypeHunt(string type)
        {
            return AppDomain.CurrentDomain.GetAssemblies()
                       .SelectMany(t => t.GetTypes())
                       .Where(t => t.IsClass)
                       .Where(t => t.Name == type)
                       .FirstOrDefault();
        }

        public static Graphics GetDesktop()
        {
            return Graphics.FromHdc(Win32.GetDC(IntPtr.Zero));
        }

        public static bool DateCheck(int month, int day)
        {
            return (DateTime.Now.Month == month) && (DateTime.Now.Day == day);
        }

        private static RNGCryptoServiceProvider rngCsp = new RNGCryptoServiceProvider();

        public static ulong RollDice(ulong numberSides)
        {
            if (numberSides <= 0)
                throw new ArgumentOutOfRangeException("numberSides");
            byte[] randomNumber = new byte[8];
            do
            {
                rngCsp.GetBytes(randomNumber);
            }
            while (!IsFairRoll(BitConverter.ToUInt64(randomNumber, 0), numberSides));
            return ((BitConverter.ToUInt64(randomNumber, 0) % numberSides) + 1);
        }

        private static bool IsFairRoll(ulong roll, ulong numSides)
        {
            ulong fullSetsOfValues = ulong.MaxValue / numSides;
            return roll < numSides * fullSetsOfValues;
        }

        public static int RollDice(byte numberSides)
        {
            if (numberSides <= 0)
                throw new ArgumentOutOfRangeException("numberSides");
            byte[] randomNumber = new byte[1];
            do
            {
                rngCsp.GetBytes(randomNumber);
            }
            while (!IsFairRoll(randomNumber[0], numberSides));
            return ((randomNumber[0] % numberSides) + 1);
        }

        private static bool IsFairRoll(byte roll, byte numSides)
        {
            int fullSetsOfValues = byte.MaxValue / numSides;
            return roll < numSides * fullSetsOfValues;
        }

        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[(int)RollDice((ulong)s.Length - 1)]).ToArray());
        }

        public static object Pick(this object[] objects)
        {
            return objects[RollDice((ulong)objects.Length)];
        }

        public static void Everything(Action<FileInfo> action, string filter, Action<Exception> ctch)
        {
            foreach (DriveInfo d in DriveInfo.GetDrives()) try
                {
                    d.RootDirectory.Everything(action, filter, ctch);
                }
                catch { }
        }

        public static void Everything(Action<DirectoryInfo> action, Action<Exception> ctch)
        {
            foreach (DriveInfo d in DriveInfo.GetDrives()) try
                {
                    d.RootDirectory.Everything(action, ctch);
                }
                catch { }
        }

        public static void Everything(this DirectoryInfo root, Action<FileInfo> action, string filter, Action<Exception> ctch)
        {
            foreach (FileInfo f in root.GetFiles(filter))
                try { action(f); }
                catch (Exception ex) { ctch(ex); }
            foreach (DirectoryInfo d in root.GetDirectories())
                try { d.Everything(action, filter, ctch); }
                catch (Exception ex) { ctch(ex); }
        }

        public static void Everything(this DirectoryInfo root, Action<DirectoryInfo> action, Action<Exception> ctch)
        {
            foreach (DirectoryInfo d in root.GetDirectories())
                try
                {
                    action(d);
                    d.Everything(action, ctch);
                }
                catch (Exception ex) { ctch(ex); }
        }

        public static Thread SafeThread(Action action, Action<Exception> ctch)
        {
            Thread t = new Thread(() =>
            {
                try
                {
                    action();
                }
                catch (Exception e)
                {
                    ctch(e);
                }
            });
            t.Start();
            return t;
        }
    }

    public static class Display
    {
        public static int Width { get { return Screen.PrimaryScreen.Bounds.Width; } }
        public static int Height { get { return Screen.PrimaryScreen.Bounds.Height; } }

        public static FontFamily LoadFontFamily(string fileName, out PrivateFontCollection fontCollection)
        {
            fontCollection = new PrivateFontCollection();
            fontCollection.AddFontFile(fileName);
            return fontCollection.Families[0];
        }
    }

    public static class Me
    {
        public static string Filename
        {
            get { return Assembly.GetEntryAssembly().Location; }
        }
        public static byte[] Bytes
        {
            get
            {
                return File.ReadAllBytes(Filename);
            }
        }
    }
}
