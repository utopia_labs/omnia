﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Omni.Info;
using QLib;

namespace Omni.Entities
{
    public abstract class Entity : IRecvMessage
    {
        public bool Fire(Message m)
        {
            try
            {
                switch (m.Class)
                {
                    case "transfer":
                        return _Transfer(m.Sender, (ulong)m.Args[0]);
                    case "grant":
                        return _Grant(m.Sender, (ulong)m.Args[0], (SID)m.Args[1]);
                    case "deny":
                        return _Deny(m.Sender, (ulong)m.Args[0]);
                    case "destroy":
                        return _Destroy(m.Sender);
                    default:
                        return OnFire(m);
                }
            }
            catch
            {
                return false;
            }
        }

        protected virtual bool OnFire(Message m) => false;

        public ulong ID { get; protected set; }
        public ulong Parent { get; protected set; }

        protected Dictionary<ulong, SID> SIDs = new Dictionary<ulong, SID>();

        public Entity(ulong parent, bool root = false)
        {
            if (root)
            {
                ID = 0;
                Parent = 0;
            }
            else
            {
                bool unique = false;
                while (!unique)
                {
                    ID = Utility.RollDice(ulong.MaxValue);
                    unique = UnusedID(ID);
                }
                Parent = parent;
                _Grant(ID, parent, new SID(true, true));
                Universe.Instance._InformCreation(this);
            }
        }

        protected bool CanModify(ulong id) => id == ID || id == Parent || SIDs[id].CanModify;
        protected bool CanDestroy(ulong id) => id == ID || id == Parent || SIDs[id].CanDestroy;

        private bool _Transfer(ulong caller, ulong newparent)
        {
            if (caller == Parent)
            {
                Parent = newparent;
                return true;
            }
            return false;
        }

        private bool _Grant(ulong caller, ulong ent, SID perms)
        {
            if (CanModify(caller))
            {
                SIDs[ent] = perms;
                return true;
            }
            return false;
        }

        private bool _Deny(ulong caller, ulong ent)
        {
            if (CanModify(caller))
            {
                SIDs.Remove(ent);
                return true;
            }
            return false;
        }

        private bool _Destroy(ulong caller)
        {
            if (CanDestroy(caller))
            {
                OnDestroy();
                Universe.Instance._InformDestruction(ID);
                return true;
            }
            return false;
        }

        protected virtual void OnDestroy() { }

        public IEnumerable<ulong> Children
        {
            get
            {
                return Universe.Instance.Entities.Values.Where(x => x.Parent == ID).Select(x => x.ID);
            }
        }

        public static Entity ByID(ulong id)
        {
            try
            {
                return Universe.Instance.Entities[id];
            }
            catch
            {
                return null;
            }
        }

        public static IEnumerable<Entity> ByClass(string _class) => 
            Universe.Instance.Entities.Values.Where(x => x.GetType().Name == _class);

        public static IEnumerable<Entity> ByName(string name)
        {
            IEnumerable<Entity> vals = Universe.Instance.Entities.Values;
            List<Entity> ret = new List<Entity>();
            foreach (Entity v in vals)
            {
                if (v is INamed)
                {
                    string v_name = ((INamed)v).Name;
                    if (v_name == name)
                    {
                        ret.Add(v);
                    }
                }
            }
            return ret;
        }

        public static bool UnusedID(ulong id) => !Universe.Instance.Entities.ContainsKey(id);
    }
}
