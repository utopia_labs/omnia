﻿using Omni.Info;
using QLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omni.Entities
{
    public class Player : Entity, INamed, IPhysical
    {
        public string Name { get; protected set; } = "Anon";
        public Coords Position { get; protected set; }

        public Player(ulong parent) : base(parent)
        {
            Position = new Coords(Utility.RollDice(64) - 32, Utility.RollDice(64) - 32);
        }

        protected bool GoToWorld(ulong h_world) => SetWorld(h_world);

        internal bool SetWorld(ulong h_world)
        {
            Entity e_world = ByID(h_world);
            if (e_world.GetType() == typeof(World))
            {
                Parent = h_world;
                return true;
            }
            return false;
        }
    }
}
