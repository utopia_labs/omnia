﻿using Omni.Info;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omni.Entities
{
    public interface INamed
    {
        string Name { get; }
    }

    public interface IRecvMessage
    {
        bool Fire(Message msg);
    }

    public interface ISpawnable { }

    public interface IPhysical
    {
        Coords Position { get; }
    }
}
