﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Omni.Info;

namespace Omni.Entities
{
    public class Catgirl : Emoton, INamed, ISpawnable
    {
        protected override bool OnFire(Message m)
        {
            switch (m.Class)
            {
                case "pet":
                    
                default:
                    return base.OnFire(m);
            }
        }

        public string Name { get; private set; } = "Unnamed Catgirl";
        public ulong Master { get; private set; } = 0;

        public Catgirl(ulong parent) : base(parent)
        {
            
        }
    }
}
