﻿using Omni.Info;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omni.Entities
{
    public abstract class Emoton : Auton
    {
        readonly Personality personality = new Personality();
        Emotions g_emotions = new Emotions();
        Dictionary<ulong, Disposition> e_disp = new Dictionary<ulong, Disposition>();

        public Emoton(ulong parent) : base(parent)
        {

        }

        protected override ushort Inflict(ulong attacker, ushort damage)
        {
            int d_ratio = damage / HP;
            return base.Inflict(attacker, damage);
        }
    }
}
