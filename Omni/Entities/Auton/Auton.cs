﻿using Omni.Action;
using Omni.Info;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omni.Entities
{
    public abstract class Auton : Entity
    {
        public ushort HP { get; protected set; } = 100;

        public Auton(ulong parent) : base(parent, false)
        {

        }

        protected virtual ushort Inflict(ulong attacker, ushort damage)
        {
            if (HP > damage)
            {
                return HP -= damage;
            }
            OnDie();
            return HP = 0;
        }

        protected virtual void OnDie()
        {
            //_Destroy(ID);
            this.Destroy(this);
        }
    }
}
