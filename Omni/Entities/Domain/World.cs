﻿using Omni.Action;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omni.Entities
{
    public class World : Domain, INamed
    {
        public string Name { get; private set; }
        public ulong Governor { get; private set; }

        public World(ulong parent, string name, ulong governor) : base(parent)
        {
            Name = name;
            Governor = governor;
        }

        protected override void OnDestroy()
        {
            while (Children.Count() > 0)
            {
                this.Destroy(Children.FirstOrDefault());
            }
        }
    }
}
