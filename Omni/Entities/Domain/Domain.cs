﻿using Omni.Action;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omni.Entities
{
    public abstract class Domain : Entity
    {
        public Domain(ulong parent, bool root = false) : base(parent, root) { }

        /*
        public Entity Spawn(ulong caller, Type t, params object[] args)
        {
            try
            {
                if (CanModify(caller))
                {
                    Entity n_ent = (Entity)Activator.CreateInstance(t, args);
                    return n_ent;
                }
            }
            catch { }
            return null;
        }
        */

        //public T Spawn<T>(ulong caller) where T : Entity => (T)Spawn(caller, typeof(T));

        public bool DestroyChild(ulong caller, ulong entity)
        {
            if (CanModify(caller))
                if (Children.Contains(entity))
                {
                    this.Destroy(entity);
                    return true;
                }
            return false;
        }
        
        protected override void OnDestroy()
        {
            foreach (ulong e in Children)
            {
                this.Transfer(ByID(e), Parent);
            }
        }
    }
}
