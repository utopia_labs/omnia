﻿using Omni.Info;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omni.Entities
{
    public class Universe : Domain
    {
        public static InitInfo Init()
        {
            Operator console = new Operator(Instance.ID);
            World new_earth = new World(Instance.ID, "New Earth", console.ID);
            console.SetWorld(new_earth.ID);
            return new InitInfo(console, new_earth);
        }

        public static Universe Instance = new Universe();

        public Dictionary<ulong, Entity> Entities = new Dictionary<ulong, Entity>();

        public Universe() : base(0, true)
        {
            Entities[0] = this;
        }

        internal void _InformCreation(Entity e)
        {
            Entities[e.ID] = e;
            Console.WriteLine(e.GetType().Name + " created [" + e.ID + "]");
        }
        internal void _InformDestruction(ulong ent)
        {
            Entity e = ByID(ent);
            Console.WriteLine(e.GetType().Name + " destroyed [" + e.ID + "]");
            Entities.Remove(ent);
        }

        protected override void OnDestroy()
        {
            throw new Exception("Universe was destroyed! This should never happen!");
        }
    }
}
