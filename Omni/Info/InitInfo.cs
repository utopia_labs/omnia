﻿using Omni.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omni.Info
{
    public struct InitInfo
    {
        public ulong Op;
        public ulong Home;

        public InitInfo(Operator god, World home)
        {
            Op = god.ID;
            Home = home.ID;
        }
    }
}
