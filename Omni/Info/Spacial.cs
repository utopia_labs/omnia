﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omni.Info
{
    public struct Coords
    {
        public long X;
        public long Y;

        public Coords(long x, long y)
        {
            X = x;
            Y = y;
        }
    }
}
