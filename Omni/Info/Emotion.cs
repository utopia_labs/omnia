﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QLib;

namespace Omni.Info
{
    public class Personality
    {
        public static short GeneratePV() => (short)((byte.MaxValue / 2) - (byte)Utility.RollDice(byte.MaxValue));

        public readonly short Open = GeneratePV();
        public readonly short Conscientious = GeneratePV();
        public readonly short Extraverted = GeneratePV();
        public readonly short Agreeable = GeneratePV();
        public readonly short Neurotic = GeneratePV();
    }

    public class Disposition
    {
        public short Positive = 0;
        public short Anxiety = 0;
    }

    public class Emotions
    {
        public short Joy = 0;       // Negative is sadness
        public short Surprise = 0;  // Negative is anticipation
        public short Trust = 0;     // Negative is disgust
        public short Anger = 0;     // Negative is fear
    }
}
