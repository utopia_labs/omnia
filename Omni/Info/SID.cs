﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omni.Info
{
    public struct SID
    {
        public bool CanModify;
        public bool CanDestroy;

        public SID(bool modify = false, bool destroy = false)
        {
            CanModify = modify;
            CanDestroy = destroy;
        }
    }
}
