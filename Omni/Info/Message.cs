﻿using Omni.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omni.Info
{
    public class Message
    {
        public ulong Sender { get; private set; }
        public ulong Receiver { get; private set; }

        public string Class;
        public object[] Args;

        public Message(ulong sender, ulong receiver)
        {
            Sender = sender;
            Receiver = receiver;
        }

        public bool Send(string m_class = "message_generic", params object[] m_args)
        {
            Class = m_class;
            Args = m_args;
            return Entity.ByID(Receiver).Fire(this);
        }
    }
}
