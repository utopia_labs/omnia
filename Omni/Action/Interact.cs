﻿using Omni.Entities;
using Omni.Info;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omni.Action
{
    public static partial class Extensions
    {
        public static bool Transfer(this Entity caller, ulong child, ulong newparent) =>
            caller.Transfer(Entity.ByID(child), newparent);
        public static bool Transfer(this Entity caller, Entity child, ulong newparent) =>
            new Message(caller.ID, child.ID).Send("transfer", newparent);

        public static bool Grant(this Entity caller, ulong child, ulong subject, SID perms) =>
            caller.Grant(Entity.ByID(child), subject, perms);
        public static bool Grant(this Entity caller, Entity child, ulong subject, SID perms) =>
            new Message(caller.ID, child.ID).Send("grant", subject, perms);

        public static bool Deny(this Entity caller, ulong child, ulong subject) =>
            caller.Deny(Entity.ByID(child), subject);
        public static bool Deny(this Entity caller, Entity child, ulong subject) =>
            new Message(caller.ID, child.ID).Send("deny", subject);

        public static bool Destroy(this Entity caller, ulong child) =>
            caller.Destroy(Entity.ByID(child));
        public static bool Destroy(this Entity caller, Entity child) =>
            new Message(caller.ID, child.ID).Send("destroy");
    }
}
