﻿using Omni.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omni.Action
{
    public static partial class Extensions
    {
        public static void GetInfo(this Entity ent)
        {
            Console.WriteLine("Class: {0}", ent.GetType().Name);
            Console.WriteLine("ID: {0}", ent.ID);
            Console.WriteLine("Parent: {0}", ent.Parent);
            Console.WriteLine("Name: {0}", ent is INamed ? (ent as INamed).Name : "[entitiy is not INamed]");
        }

        public static void PrintCollection<T>(this IEnumerable<T> ienum)
        {
            foreach (T o in ienum)
            {
                if (o is Entity)
                {
                    Entity o_ent = o as Entity;
                    Console.WriteLine("{0} [{1}]", o_ent.ID, o_ent.GetType().Name);
                    continue;
                }
                Console.WriteLine(o.ToString());
            }
        }
    }
}
