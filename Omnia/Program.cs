﻿using IronPython;
using IronPython.Hosting;
using Microsoft.Scripting.Hosting;
using Omni.Entities;
using Omni.Info;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Omni
{
    class Program
    {
        static void Main(string[] args)
        {
            InitInfo info = Universe.Init();
            ScriptEngine p_engine = Python.CreateEngine();
            dynamic p_scope = p_engine.CreateScope();
            p_scope.Root = Universe.Instance;
            p_scope.Home = Entity.ByID(info.Home);
            p_scope.Me = Entity.ByID(info.Op);
            p_engine.Execute(@"import clr
clr.AddReference(""Omni"")
from Omni import *
from Omni.Entities import *
from Omni.Info import *
from Omni.Action import *
clr.ImportExtensions(Extensions)", p_scope);
            while (true)
            {
                try
                {
                    Console.Write("OMNI>");
                    p_engine.Execute(Console.ReadLine(), p_scope);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }
    }
}
